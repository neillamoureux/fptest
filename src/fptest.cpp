/*
 ============================================================================
 Name        : fptest.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, const char* argv[]) {
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
