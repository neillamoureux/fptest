/*
 * code.c
 *
 *  Created on: Jan 6, 2015
 *      Author: albertasat
 */
#include "code.h"
#include <stdio.h>

static void method_a()
{
	printf("derp\n");
}
static void method_b(int* a, int b){
	*a = b;
}
void new_method_container(struct method_container_t* obj){
	obj->method_a = method_a;
	obj->method_b = method_b;
}

