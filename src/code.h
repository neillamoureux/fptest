/*
 * code.h
 *
 *  Created on: Jan 6, 2015
 *      Author: albertasat
 */

#ifndef CODE_H_
#define CODE_H_

struct method_container_t
{
	void (*method_a)(void);
	void (*method_b)(int*, int);
};

void new_method_container(struct method_container_t* obj);

#endif /* CODE_H_ */
